# Common tools

- `notify-send` Use to send notification from bash
- `zenity` Display GTK dialog box and return user input
- [Butterfly](https://github.com/paradoxxxzero/butterfly) web server for terminal
- [Black Screen](https://github.com/shockone/black-screen) nodejs terminal
- [hyperterm](https://github.com/zeit/hyperterm) another nodejs terminal

## Fonts
- [Source Code Pro](https://github.com/adobe-fonts/source-code-pro)
- [Fira Code](https://github.com/tonsky/FiraCode)

## Using *Compose* key

Keys are set in `/usr/share/X11/locale/en_US.UTF-8/Compose`

Key name can be seen via `xev | fgrep "keysym"`
- `o c`	© (0169)	
- `t m`	™ (0153)
- `o r`	® (0174)
- `< <`	« (0171)
- `> >`	» (0187)
- `" <`	“ (0147)
- `" >`	” (0148)
- `. .`	… (0133)
- `- -`	— (0151)
- `x x`	× (0215)
- `- :`	÷ (0247)
- `- +`	± (0241)
- `_ >` ≥ (0242)
- `_ <`	≤ (0243)
- `~ ~`	≈ (0248)
- `^ 0`	⁰ (0186)
- `^ 1`	¹ (0185)
- `^ 2`	² (0178)
- `^ 3`	³ (0179)
- `1 2`	½ (0188)
- `1 4`	¼ (0189)
- `3 4`	¾ (0190)
- `o o`	° (0176)
- `% o` ‰ (0137)
- `8 8` ∞ 
- `m u` µ
- `/ =` ≠

## XFCE4
- To set up windows positions and sizes, use `devilspie` [documentation](http://www.foosel.org/linux/devilspie)
  - Get current window list `xlsclients -l`
  - Get their geometry `xwininfo`
- To disable the bracketed paste mode `printf "\e[?2004l"`
- To add new app to menu `exo-desktop-item-edit --create-new ~/.local/share/applications/`

## Gnome

### Configure window title bar height

Create ~/.config/gtk-3.0/gtk.css:
```css
.header-bar.default-decoration {
    padding-top: 3px;
    padding-bottom: 3px;
}

.header-bar.default-decoration .button.titlebutton {
    padding-top: 2px;
    padding-bottom: 2px;
}

```
### Configure title bar buttons

```
gconftool-2 --set /apps/metacity/general/button_layout  --type string "menu:minimize,maximize,close"
```

# Hardware

## Hardware Sensors

```bash
sudo add-apt-repository ppa:alexmurray/indicator-sensors
sudo apt-get install indicator-sensors
```

##Improve window management button appearance:

```bash
sudo apt-get install shiki-colors-metacity-theme
gconftool-2 -s --type string /apps/metacity/general/theme Shiki-Colors-Metacity
gsettings set org.gnome.libgnomekbd.keyboard options "['caps\tcaps:none']"
```

##Laptop

Preventing the computer from suspending when closing the lid: `echo "HandleLidSwitch=ignore" >> /etc/systemd/logind.conf`
