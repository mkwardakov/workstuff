# Bash

## Scripting
- `#!/bin/sh -ef` Works more simple than bash
- `[ $( date -d 'next day' +%d ) == 1 ] && echo 'its the end of the month'` True on last day of the month
- `for i in ./*.mp3; do … done` Failproof files iteration
- `diff <(./myscript.sh) expected_output.txt` Check if the script went wrong
- `diff -e` Produces an ed program (ancestor of `sed`)

### Special Variables
- `$*` List of script parameters in one line
- `$@` List of script parameters one per line
- `$#` Number of positional parameters
- `$?` Exit/return code of the most recently executed foreground-pipeline
- `$$` The process ID (PID) of the parent process, `$BASHPID`
- `$!` The process ID (PID) of the most recently executed background pipeline (like started with command &)
- `$0` The name of the shell or the shell script (filename). Set by the shell itself.
- `$_` Expands to the last argument to the previous command

## Hotkeys
- `CTRL+A` Cursor to start of line
- `CTRL+D` Terminate terminal session
- `CTRL+E` Cursor to end of line
- `CTRL+K` Delete right of the cursor
- `CTRL+L` Clear the terminal
- `CTRL+R` Search in history backwards
- `CTRL+T` Swap last two characters
- `CTRL+U` Delete everything left of the cursor
- `CTRL+W` Delete everything after last space
- `CTRL+Y` Paste deleted string
- `CTRL+XE` Run editor
- `Ctrl-|` (**SIGQUIT** signal) to a running ping command in Linux will print out brief summary output
- `Ctrl-_` Incremental undo
- `ALT+BACKSPACE` Delete one word
- `ALT+B` Back one word
- `ALT+F` Forward one word
- `ALT+T` Swap last two words
- `ALT+.` or `ESC+.` or `!$` Adds last argument of recent commands

## Tips
- Running command with preceding space doesn’t go to the history
- `> file.txt` New empty file
- `^foo^bar` Run previous command with substring replaced
- `<<<` or `echo 'foo' | cmd` Send stdin to process right from shell
- Очень многие утилиты умеют читать с **stdin**, если им в качестве файла указать `-`
- `\rm file` Run the non-aliased version of rm. Putting `\` in front of a command name will bypass any aliases of the same name; also `type rm`
- Expandos: `!$`, `!^`, `!*`, `!#` and `!:2` 
- Add `bind Space:magic-space` to .bashrc and any combination will be automatically expanded when you hit space
- You can invoke system default editor from within less, by pressing **v**
- `export TMOUT=300` Set session to close after 5 minutes of idle

## Prompt
- Show last command run time
```
function timer_start {
  timer=${timer:-$SECONDS}
}
function timer_stop {
  timer_show=$(($SECONDS - $timer))
  unset timer
}
trap 'timer_start' DEBUG
if [ "$PROMPT_COMMAND" == "" ]; then
  PROMPT_COMMAND="timer_stop"
else
  PROMPT_COMMAND="$PROMPT_COMMAND; timer_stop"
fi
PS1='[last: ${timer_show}s][\w]$ '
```
