# Working in Linux

## Everyday tasks

- Use `fasd` to move through filesystems effectively; [repo](https://github.com/clvv/fasd)
- Stop running process `CTRL+Z` and send it to background bg then `disown` to unbound from the current shell. `command; disown -a && exit`
- `function cdl { cd "$@" && ls -thor; }` Cd to a folder and list it; also see `tree`
- `watch -n 10 '{ uptime ; free ; df -h; }'` Watch a few commands together every 10 seconds
- `cd -` Cd to previous directory
- `ls -X` Listing sorted alphabetically by extension

## File processing

- `file` Show file format
- To show progress bar when copying use `rsync`, `vcp` and `pycp`
- `rename 's/text_to_find/been_renamed/' *.txt` Mass rename
- `cut -d, -f1,5,10-15 data.csv > new.csv` Use cut to print out columns 1, 5 and 10 through 15 in data.csv and write that to new.csv
- `tac access.log | less` page through a log file in reverse to avoid guessing number of lines to tail
- `pdftk` merges several pdf files into one
- `uniq .bash_history | sponge .bash_history` read from and write to the same file
## More command line tools
- `pycp` copy with progress bar
- `icdiff` side-by-side diffs
- `pandoc` convert file from one markdown format to any other: md, html, docx, latex, pdf, epub
- `fuck` corrects your last ‘command not found’
- `how2` provides search through stackoverflow in bash like man (https://github.com/santinic/how2)
- `antiword` [console word viewer](http://www.winfield.demon.nl/)

### grep

- `grep -v 'DEBUG' some.log` skip debug messages
- `grep -E -v -f expressions.conf file.txt` print lines not matching any extended regular expressions in expressions.conf
- `\K` operator causes the preceding pattern to match, but doesn't include the match in the result: `grep -Po 'VERSION: \K[0-9]{1,3}\.[0-9]{1,3}' MANIFEST.MF` (Perl regexp)

### sed

[Sed online debugger](http://aurelio.net/projects/sedsed/)

- `find . -name "*.css" -exec sed -i -r 's/#(FF0000|F00)/#0F0/' {} \;` Find all css files and replace red with green in them
- `sed -e '/parameter/s/oldvalue/newvalue/' old.cfg >new.cfg` Change a parameter in config; see sed one-liners explained and tutorial as well
- `sed '/^$/d' file.txt` Delete blank lines

### awk: see awk [one-liners explained](http://www.catonmat.net/blog/awk-one-liners-explained-part-one/) and [tutorial](http://www.tutorialspoint.com/awk/index.htm)

- `dpkg -l | awk '{printf "%s-%s\n", $2, $3}'` Short list of installed debian packages
- `ps ax | awk '{if ($4!="0:00") print $4 "\t" $1 "\t" $5}' | sort -gr | head` Show top 10 processor time consuming processes
- `history | awk 'BEGIN {FS="[ \t]+|\\|"} {print $3}' | sort | uniq -c | sort -nr | head` Top 10 commands you use
- `awk '!seen[$0]++' app.log` Remove duplicate lines from a file

## Utilities

- `cal` Console calendar
- `id` Get real and effective user and group permissions
- `factor 256` Prints prime factors of 256
- `ascii` Quick access to the ASCII character table
- `fc -nl -10 0 > script.sh` Put last 10 commands to the file
- `time` Use before any command to get running time
- `mount | column -t` Format output as table
- `du -sh */ | sort -h` Show directory size and sort by size, also `ncdu`
- `wc -l some.log` Count strings in a file
- `less +F filename.log` just like `tail -f` but with `less` features
- `tput` Control terminal features and output: cursor position, color etc.
- `dialog` App implements interacting with user
- `httpie` Human-friendly curl replacement

## Pranks

Coworker's unattended Mac:
```
crontab -e
*/30 * * * * say -v whisper "I am watching you"
```
