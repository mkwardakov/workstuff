#!/usr/bin/env bash
rofi -show drun -modi "drun,combi,calc:qalc +u8 -b 10 -nocurrencies,batz:~/.config/i3/batz.sh" -font "Inconsolata 16" -width 40 -bw 0 -show-icons -sidebar-mode -terminal qterminal -theme gruvbox-dark-hard
