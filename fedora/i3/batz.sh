#!/usr/bin/env bash
set -eo pipefail
declare -A TIME_ZONES

## Change the default timezones here!
TIME_ZONES=(
    ["UTC"]="UTC"
    ["California"]="America/Los_Angeles"
    ["Denver"]="America/Denver"
    ["Moscow"]="Europe/Moscow"
)

function c() {
    BOLD='' #'\033[1m'
    NONE='' #'\033[00m'
    RED='' #'\033[01;31m'

    case $1 in
        bold)
            color=$BOLD
            ;;
        normal)
            color=$NONE
            ;;
        red)
            color=$BOLD$RED
            ;;
        *)
    esac
    printf "%b" "${color}$2${NONE} "
}

currenttz=$(env ls -l /etc/localtime|env awk -F/ '{print $(NF-1)"/"$NF}')
date=date
type -p gdate >/dev/null 2>/dev/null && date=gdate

[[ -e /usr/share/zoneinfo/${currenttz} ]] || { echo "${currenttz} does not exist in /usr/share/zoneinfo" ;}

for i in ${!TIME_ZONES[@]};do
    res=$(TZ=${TIME_ZONES[$i]} ${date})

    if [[ $currenttz == ${TIME_ZONES[$i]} ]]; then
        if [[ -n $specified ]]; then
            specified="✈"
        else
            specified="🏠"
        fi
        printf "%s%-15s: %s %s\n" "$emoji" `c bold ${i}` "$res" $specified
    else
        printf "%s%-15s: %s\n" "$emoji" `c bold $i` "$res"
    fi
done
