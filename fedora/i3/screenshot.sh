#!/usr/bin/env bash
#set -e
# To get new token, at browser:
# https://api.imgur.com/oauth2/authorize?client_id=e33c9ce44b4b06a&response_type=token
# to create album
# http POST https://api.imgur.com/3/album Authorization:"Bearer $token" title=Screenshots privacy=hidden
# to get alubm list:
# http GET https://api.imgur.com/3/account/mkwardakov/albums/ids Authorization:"Bearer $token"
# to refresh
# http POST https://api.imgur.com/oauth2/token refresh_token= client_id= client_secret= grant_type=refresh_token

tstamp=$(date +'%Y%m%d-%H%M%S')
fname="$HOME/Downloads/screenshots/screenshot-$tstamp.png"
gnome-screenshot -aB --file="$fname"
if [[ "$1" != "local" && -f "$fname" ]]; then
    token="3e128b4745d793abe7c5933c426d933321f9e843"
    album="3I5XHwA"
    base64 -w0 "$fname" > "$fname.b64"
    up=$(http --ignore-stdin --body POST https://api.imgur.com/3/image Authorization:"Bearer $token" "album=$album" "image=@$fname.b64")
    if [[ "$?" == "0" ]]; then
        link=$(echo $up | jq -r .data.link)
        if [[ "$?" == "0" ]]; then
            echo "$link" > "$fname.link"
            echo "$link" | xsel -i
        else # Show user what just went wrong
            echo "$up" > "$fname.log"
            echo "$up"   | xsel -i
        fi
    elif [[ "$?" == "403" ]]; then # Token expired
        touch "$fname.FAIL"
        echo "Update your IMGUR access token!" | xsel -i
    fi
    rm "$fname.b64"
fi
