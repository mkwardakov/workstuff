#!/usr/bin/env bash
# https://www.reddit.com/r/unixporn/comments/3358vu/i3lock_unixpornworthy_lock_screen/
# https://www.reddit.com/r/unixporn/comments/4yj29e/i3lock_simple_blur_script/
set -x
set -e
lockfile='/tmp/lock_screen.png'
pattern="$(\ls -1 ~/.config/i3/*.png | shuf -n1)"
[[ -f $lockfile ]] && rm $lockfile
[[ -f $pattern ]] && ffmpeg -loglevel quiet -i <(import -silent -window root png:-) -i "$pattern" -y -filter_complex "boxblur=7:5,overlay=(main_w-overlay_w-1):(main_h-overlay_h-1)" -vframes 1 $lockfile
if [[ -f $lockfile ]]; then
    i3lock --ignore-empty-password --image=$lockfile
else
    i3lock --ignore-empty-password -c 000000
fi
