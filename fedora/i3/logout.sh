#!/usr/bin/env bash
Poweroff_command="systemctl poweroff"
Reboot_command="systemctl reboot"
Logout_command="dm-tool switch-to-greeter"
Suspend_command="$HOME/bin/suspend"
Hibernate_command="$HOME/bin/hibernate"
Lock_command="$HOME/bin/lock"
StopX_command="xkill"
Exit_command="i3-msg exit"
Cancel_command=""
rofi_command="rofi -i -theme gruvbox-dark-hard -l 9 -width 8"
options=$' Lock\n Reboot\n  Hibernate\n  Suspend\n Poweroff\n Logout\n  Exit\n Restart X\n Cancel'
eval \$"$(echo "$options" | $rofi_command -dmenu -p "" | awk '{print $2}')_command"
