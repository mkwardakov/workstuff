#!/usr/bin/env bash
set -e
width=$(xrandr | grep '*' | awk '{print $1}' | cut -f1 -dx)
height=$(xrandr | grep '*' | awk '{print $1}' | cut -f2 -dx)
ffmpeg -y \
    -i "$HOME/.wallpapers/wp07.jpg" \
    -i "$HOME/.wallpapers/left.png" \
    -i "$HOME/.wallpapers/right.png" \
    -filter_complex "[0] scale=1920:1050 [0s];
                     [1] scale=-1:1050 [1s];
                     [2] scale=-1:1050 [2s];
                     [0s][1s] overlay=0:0 [3];
                     [3][2s] overlay=(main_w-overlay_w):0" \
    "$HOME/.wallpapers/rendered.png"
nitrogen --set-tiled --save "$HOME/.wallpapers/rendered.png"
