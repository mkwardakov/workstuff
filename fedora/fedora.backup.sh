#!/usr/bin/env bash
cd $HOME
crontab -l > .config/crontab
pushd repos
rm -f list
for rep in $(ls -A); do
  pushd "$rep"
  git remote -v >> ../list
  popd
done
popd
zip --symlinks --recurse-paths --password 'eiSon8wei@Ph8ei7ThaedutaepheeBah' "backup.zip" \
  /etc/cron.daily/ \
  /etc/sddm/ \
  /etc/yum.repos.d/ \
  /usr/share/sddm/themes/ \
  bin/ \
  vpn/ \
  repos/list \
  .config/gtk-3.0/ \
  .config/gtk-4.0/ \
  .config/i3/ \
  .config/mc/ \
  .config/morc_menu/ \
  .config/nautilus/ \
  .config/onedrive/ \
  .config/nitrogen/ \
  .config/qterminal.org/ \
  .config/ranger/ \
  .config/sublime-text-3/ \
  .config/syncthing/ \
  .config/vlc/ \
  .config/yandex-disk/ \
  .config/crontab \
  .fonts/ \
  .gnupg/ \
  .icons/ \
  .moc/ \
  .oh-my-zsh/ \
  .ssh/ \
  .themes/ \
  .vim/ \
  .wallpapers/ \
  .bashrc \
  .fasd \
  .gitconfig \
  .rtorrent.rc \
  .tmux.conf \
  .vimrc \
  .zsh_history \
  .zshrc \
  *.conf \
  --exclude .uuid \
            .wine/drive_c/homm \
            .moc/socket2
mv "backup.zip" "$HOME/files/Yandex.Disk/$(hostname)-backup-$(date +%Y%m%d).zip"
