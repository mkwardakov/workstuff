#!/usr/bin/env bash
# sudo vpnc --pid-file /run/vpnc.pid $HOME/vsk-vpnc.conf
# sudo openvpn --config ~/hostsailor-client.conf --writepid /run/openvpn.pid --log /var/log/openvpn.log --daemon
sudo dnf install https://download1.rpmfusion.org/{free/fedora/rpmfusion-free,nonfree/fedora/rpmfusion-nonfree}-release-$(rpm -E %fedora).noarch.rpm
sudo dnf remove vim-minimal
sudo dnf update
sudo dnf install \
  kernel-devel kernel-headers gcc make dkms acpid \
  libglvnd-glx libglvnd-opengl libglvnd-devel pkgconfig \
  python3-pip python3-devel autoconf automake libtool redhat-rpm-config util-linux-user \
  strace the_silver_searcher ccze lshw prename nmon htop iotop colordiff dstat glances lsof \
  atool ncdu pwgen tiptop tmux tree lm_sensors \
  mtr telnet wget curl iftop ngrep rsync \
  openvpn wireguard-tools vpnc git git-extras tig jq httpie \
  i3 i3lock nitrogen xsel network-manager-applet pavucontrol blueberry \
  fontawesome-fonts light rofi compton qalculate conky conky-manager \
  flac ffmpeg rtorrent moc ranger mc antiword gcolor2 flameshot \
  wine winetricks sddm qt5-qtquickcontrols2 zsh vim \
  bridge-utils libvirt virt-install qemu-kvm libvirt-devel virt-top libguestfs-tools
  nemo gwenview qterminal vlc steam remmina \
  chromium chromium-libs-media-freeworld
touch ~/timidity.cfg
sudo ln -s /usr/bin/vim /usr/bin/vi
chsh -s /usr/bin/zsh $(whoami)
pip install --user netifaces xkbgroup xq
sudo pip install youtube-dl
# Change display manager, set appearance
sudo systemctl disable gdm
sudo systemctl stop gdm
sudo systemctl enable sddm
sudo systemctl start sddm
sudo dnf install lxappearance
# Skype
wget https://go.skype.com/skypeforlinux-64.rpm
sudo dnf install skypeforlinux-64.rpm
# Yandex Disk
wget http://repo.yandex.ru/yandex-disk/rpm/stable/x86_64/yandex-disk-0.1.5.1010-1.fedora.x86_64.rpm
sudo dnf install yandex-disk-0.1.5.1010-1.fedora.x86_64.rpm
# Sublime 3
sudo rpm -v --import https://download.sublimetext.com/sublimehq-rpm-pub.gpg
sudo dnf config-manager --add-repo https://download.sublimetext.com/rpm/stable/x86_64/sublime-text.repo
sudo dnf install sublime-text
# ZFS
sudo dnf install http://download.zfsonlinux.org/fedora/zfs-release$(rpm -E %dist).noarch.rpm
gpg --quiet --with-fingerprint /etc/pki/rpm-gpg/RPM-GPG-KEY-zfsonlinux
sudo dnf install kernel-devel zfs
# Docker CE
sudo dnf remove zfs-fuse
sudo dnf install dnf-plugins-core
sudo dnf config-manager --add-repo https://download.docker.com/linux/fedora/docker-ce.repo
sudo dnf install docker-ce docker-ce-cli containerd.io
curl -OL https://github.com/wagoodman/dive/releases/download/v0.7.1/dive_0.7.1_linux_amd64.rpm
sudo rpm -i dive_0.7.1_linux_amd64.rpm
# RVM & Ruby
gpg2 --recv-keys 409B6B1796C275462A1703113804BB82D39DC0E3 7D2BAF1CF37B13E2069D6956105BD0E739499BDB
curl -sSL https://get.rvm.io | bash -s stable
rvm install 2.6 #gem install pry
# SPECTRE
grep . /sys/devices/system/cpu/vulnerabilities/*
sudo sed -i 's/quiet/quiet pti=off l1tf=off nospec_store_bypass_disable no_stf_barrier nospectre_v1 nospectre_v2/' /etc/sysconfig/grub
sudo grub2-mkconfig -o /boot/grub2/grub.cfg
sudo reboot
# NVidia https://www.if-not-true-then-false.com/2015/fedora-nvidia-guide/
inxi -G --display
chmod +x NVIDIA-Linux-*.run
sudo dnf install \
  kernel-devel \
  kernel-headers \
  gcc \
  make \
  dkms \
  acpid \
  libglvnd-glx \
  libglvnd-opengl \
  libglvnd-devel \
  pkgconfig
echo "blacklist nouveau" >> /etc/modprobe.d/blacklist.conf
# Append ‘rd.driver.blacklist=nouveau’ to end of ‘GRUB_CMDLINE_LINUX=”…”‘ to /etc/sysconfig/grub /boot/grub2/grub.conf
sudo grub2-mkconfig -o /boot/grub2/grub.cfg || grub2-mkconfig -o /boot/efi/EFI/fedora/grub.cfg
sudo dnf remove xorg-x11-drv-nouveau
sudo reboot
sudo ./NVIDIA-Linux-*.run
# Mouse and keyboard
setxkbmap -layout 'us,ru' && setxkbmap -option 'grp:caps_toggle'
xinput --list --short
xinput --list-props "pointer:COMPANY USB Device"
set mouse 1/4 1

# KVM
sudo systemctl enable libvirtd
sudo systemctl start libvirtd

# fzf, duf
git clone --depth 1 https://github.com/junegunn/fzf.git ~/.fzf
~/.fzf/install
wget https://github.com/muesli/duf/releases/download/v0.5.0/duf_0.5.0_linux_amd64.rpm
sudo dnf install ./duf_0.5.0_linux_amd64.rpm
#!/usr/bin/env bash
sudo rpm --import https://packages.microsoft.com/keys/microsoft.asc
curl https://packages.microsoft.com/config/rhel/7/prod.repo | sudo tee /etc/yum.repos.d/microsoft.repo
sudo sh -c 'echo -e "[code]\nname=Visual Studio Code\nbaseurl=https://packages.microsoft.com/yumrepos/vscode\nenabled=1\ngpgcheck=1\ngpgkey=https://packages.microsoft.com/keys/microsoft.asc" > /etc/yum.repos.d/vscode.repo'
sudo dnf install compat-openssl10 powershell code dotnet-sdk-2.2
