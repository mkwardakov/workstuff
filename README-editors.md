# Working with source code

## Git
### Helpful tools
- [git-up](https://github.com/aanand/git-up)
```
gem install git-up
git config --global git-up.bundler.check true
```
- [Tig](https://github.com/jonas/tig.git)
```
git clone https://github.com/jonas/tig.git
cd tig
make; make install
```

### Hints
- `git log -L 155,157:file.txt` Trace the evolution of lines 155 to 157 in the file
- `git log --color --graph --pretty=format:'%Cred%h%Creset -%C(yellow)%d%Creset %s %Cgreen(%cd) %C(bold blue)%an <%ae>%Creset' --abbrev-commit` Beautiful version of the log
- `git commit --amend` Modify the previous commit's message
- `git commit --amend -C HEAD` Update commit
- `git blame <fileName>` Who F'd it All Up?
- `git checkout -- <fileName>` Abort changes in a file
- `git log --grep regexp` Search commit logs for lines of text matching regular expression regexp
- `git push origin :<branchName>` Delete remote branch
- `git push origin +<branchName>^1:<branchName>` Delete last commit from remote branch
- `git reset HEAD^ ; git push origin +HEAD` Delete last commit from both local and remote branches
- `git reflog` shows all operations
- `git revert <commit>` undo all commits after and creates new with changes from undone commits; the only way to undo something without change commits history
- `git checkout <commit> -- <filename>` pull any file from any commit
- `git branch --merged` shows merged branches

## Sublime
- My plugins
  - [Package Control](https://packagecontrol.io)
  - [Emmet](https://github.com/sergeche/emmet-sublime): made work with tags easy
  - [Clipboard Manager](https://gist.github.com/1590661): commands `clipboard_manager*`
  - [EasyMotion](https://github.com/tednaleid/sublime-EasyMotion): hotkey `CTRL-;`
  - [Pretty JSON](https://github.com/dzhibas/SublimePrettyJson): hotkey `CTRL+ALT+J`
  - [RuboCop](https://github.com/pderichs/sublime_rubocop) commands `Rubocop*`
  - [GitGutter](https://github.com/jisaacks/GitGutter) shows git diff
  - [Modific](https://github.com/gornostal/Modific) show git diff and do stuff
  - [MarkdownEditing](https://github.com/ttscoff/MarkdownEditing)
  - [ZenTabs](https://github.com/travmik/ZenTabs)
  - [Inc-Dec-Value](https://github.com/rmaksim/Sublime-Text-2-Inc-Dec-Value): hotkey `ALT+up`/`ALT+down`
  - [Gist](https://github.com/condemil/Gist)
- My config
```
{
	"auto_complete_commit_on_tab": true,
	"color_scheme": "Packages/Color Scheme - Default/Cobalt.tmTheme",
	"default_line_ending": "unix",
	"draw_white_space": "all",
	"font_face": "Source Code Pro",
	"font_size": 13,
	"ignored_packages":
	[
	],
	"tab_size": 2,
	"translate_tabs_to_spaces": true,
	"vintage_ctrl_keys": true,
	"vintage_start_in_command_mode": true
}
```

## VIM

### Links
- [Learn vim progressively](https://www.evernote.com/shard/s16/sh/2a67f421-3b83-4dc0-a035-ba42333d7b3e/23c19877a14fd6f29d7dd54fb2183b91)
- [Cream for vim](http://cream.sourceforge.net/)
- [Interactive tutorial](http://www.openvim.com/)
- [Mastering the Vim Language](https://www.youtube.com/watch?v=6T5aCzbrd18) 
- [The Art of Vim](https://upcase.com/the-art-of-vim)
- [modeline doc](http://vim.wikia.com/wiki/Modeline_magic) In-file vim settings

### Command-line options
- `vim filename +lineNumber` open Vim with the cursor in a certain line
- vim merge tool `vim -d[oOp] file1.txt file2.txt`

### Tips
- `CTRL+K` from insert mode followed by the digraph combination (see `:dig`) gives us almost any character
- Search and repace all `:%s/old/new/`
- Calc in insert mode `CTRL-R`, then `=`, and then `120*7` finishing with **ENTER**
- `qaYpCTRL+Aq` Record a macro called 'a': Yank a line, p inserts under, increment number under cursor. Run with @a, run 8 times 8@a to get numbered list
- `do` Get changes from other window into the current window
- `dp` Put the changes from current window into the other window
- `:diffupdate` diff update
- `:w !diff % -` Display changes performed since last save
- With `:TOhtml` command, Vim turns your text document into an HTML document in a horizontal split
- `:10,15sort` will sort the next lines 10-15
- `gf` open a file with the cursor over the file name
- `gU` and `gu` uppercase/lowercase
- `CTRL+R "` inserts buffer, `CTRL+R *` inserts system clipboard

#### Block operations
- `5>>..` shifts five lines to the right, then repeats the operation twice
- `CTRL+V` select a block, press `SHIFT+I`, type text, press **ESC** and see how it affect the block; gv to reselect the block
- Same as previous `:10,12s/^/#` and delete `:10,12s/^#//` or `:10,15norm i#` and `:10,15norm ^x`
