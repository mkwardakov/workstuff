# Handbook with recipes

## Useful links

- [Powerful regexp builder](https://regex101.com/)
- [JSON formatter](http://jsonviewer.stack.hu/)
- [The art of command line](https://github.com/jlevy/the-art-of-command-line)
- [Netsarang XShell](https://www.netsarang.com/en/xshell/) for Windows

## Deploy work environment

`git config --global diff.noprefix true` it removes the silly `a/` and `b/` prefixes so that when you double-click select one to copy

### Packages to install

```
git vim mc zsh bash-completion colordiff silversearcher-ag source-highlight ccze 
tree prename linux-tools-common build-essential checkinstall maybe
httpie jq telnet mtr-tiny ngrep iftop
lsof rsync ncdu iotop htop nmon dstat glances sysstat nethogs lshw
shntools flac vlc
antiword
```
Oh-My-Zsh: `sh -c "$(curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"`


### Prepare profile

```sh
chsh /usr/bin/zsh
git clone https://github.com/clvv/fasd.git ~/repos/fasd
git clone https://github.com/zsh-users/zsh-syntax-highlighting.git ~/repos/zsh-syntax-highlighting
git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim
cp ~/repos/fasd/fasd ~/bin/fasd
sudo pip install percol howdoi
```

Tools:
- [ngrok](https://ngrok.com/) Public URLs for exposing your local web server
- [tabnine](https://www.tabnine.com/) Smart Compose for code using deep learning

# Working in Linux

See README-tips.md

# Administrating Linux

See README-admin.md

# Working with source code

See README-editors.md

# Window managers

See README-X.md
