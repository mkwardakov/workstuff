local ret_status="%(?:%{$fg_bold[cyan]%}λ:%{$fg_bold[red]%}⚡%s)%{$reset_color%}"
PROMPT='${ret_status} %{$fg[blue]%}@%m%{$reset_color%} %{$fg[green]%}›%3~/%{$reset_color%} %{$fg[magenta]%}$(git_prompt_short_sha)$(git_prompt_info)%{$reset_color%}» '
ZSH_THEME_GIT_PROMPT_PREFIX="‹%{$FG[208]%}"
ZSH_THEME_GIT_PROMPT_SUFFIX=" "
ZSH_THEME_GIT_PROMPT_DIRTY="%{$fg[red]%}*"
ZSH_THEME_GIT_PROMPT_CLEAN=""
# » λ Ξ ‹ › · ° µ 𐎄
# $(git_prompt_info)
# $(git_current_branch)
# $(git_prompt_short_sha)
# $FG[208] orange
# $FG[237] gray
