#!/bin/bash
segment='180' #seconds
for j in ./*.mp3 ;
do
#	length=$( ffmpeg -i "$j" 2>&1 | grep -oP "[0-9]{2}:[0-9]{2}:[0-9]{2}.[0-9]{2}" )
	basename=${j%.*} 
	pt1=${basename:2:4}
	pt2=${basename:7}
#	echo "$pt1 $pt2"
#	k=10
#	res=''
#	offset='00:00:00'
#	echo "Splitting file $j | length=$length"
#	while [[ "s$res" = "s" ]]; do
#		stime=$( date -u -d "$offset" +"%s" )
#		res=$( ffmpeg -i "$j" -map 0:0 -ss "$offset" -t "$segment" -c:a copy "./splitted/$pt1.$k $pt2.mp3" 2>&1 | grep 'empty' )
#		echo "Segment $k, starting at $offset| $res"
#		offset=$( date -u -d "0 $stime seconds + $segment seconds" +"%H:%M:%S" )
#		k=$(( k + 1 ))
#	done
	ffmpeg -i "$j" -c:a copy -f segment -segment_time $segment "splitted/$pt1.%03d $pt2.mp3"
done

