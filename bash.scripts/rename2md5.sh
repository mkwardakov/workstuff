#!/usr/bin/env bash
[[ -z "$1" ]] && exit 1
find "$1" -type f | grep -vE -e '.*\/[0-9a-f]{32}\..{3,4}' | grep -vE -e '.*\.(jpg|jpeg)' > filelisting
while read j ; do
    if [[ -w "$j" ]]; then
        hash=$(md5 -q "$j")
        mv -v "$j" "$hash.${j##*.}"
    fi
done < filelisting
rm -f filelisting
