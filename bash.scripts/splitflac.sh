#!/usr/bin/env bash
# Script searches all folders recurrently for FLAC/CUE files, splits flac into flac tracks and writes ID3v2 tags
# Requirements
#   * Python package: mutagen
#   * Folder albums have to be named by pattern: [$YEAR] $ALBUM
set -e

function split {
    [[ -d "$1" ]] || return "$1: Is not a directory"
    pushd "$1"
    flac=$(ls *.flac) && cue=$(ls *.cue) && echo "Get filenames"
    if [[ -f "$flac" && -f "$cue" ]]; then
        echo "splitting $flac"
        shnsplit -f "$cue" -t "%n %t" "$flac"
        flac *.wav
        rm *.wav "$flac" "$cue"
        al="${PWD##*/}";
        for j in *.flac; do
            mid3v2 --artist="David Bowie" -year="${al:1:4}" --album="${al:7}" --track="${j:0:2}" --song="${j:3:-5}" "$j"
        done
    else
        for j in ./*; do
            split $j
        done
    fi
    echo
    popd
}

for j in ./*; do
    split "$j"
done
