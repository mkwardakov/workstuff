#!/bin/bash
segment='180' #seconds
for j in ./*.mp3 ;
do
	basename=${j%.*} 
	pt1=${basename:2:4}
	pt2=${basename:7}
	ffmpeg -i "$j" -c:a copy -f segment -segment_time $segment "splitted/$pt1.%03d $pt2.mp3"
done

