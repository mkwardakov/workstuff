#!/bin/bash
# resize sources
mkdir resized
ffmpeg -r 25 -pattern_type glob -i '*.jpg' -c:v copy output.avi
cd resized

# deflicker
# sudo apt-get install libfile-type-perl libterm-progressbar-perl
# wget https://github.com/cyberang3l/timelapse-deflicker/blob/master/timelapse-deflicker.pl
#./timelapse-deflicker.pl -h
#./timelapse-deflicker.pl -v
#cd Deflickered

# build video
ffmpeg -r 25 -pattern_type glob -i '*.jpg' -c:v copy output.avi
# -q:v can get a value between 2-31. 2 is best quality and bigger size, 31 is worst quality and least size)
#ffmpeg -r 25 -pattern_type glob -i '*.jpg' -c:v mjpeg -q:v 2 output.avi
# Lossless jpeg resulting in a huuuuuuuge file. Even larger in size than the sum of all of the input pictures together in the case of jpg images.
#ffmpeg -r 25 -pattern_type glob -i '*.jpg' -c:v ljpeg output.avi

# compress the video
ffmpeg -i output.avi -c:v libx264 -preset slow -crf 15 output-final.mkv
