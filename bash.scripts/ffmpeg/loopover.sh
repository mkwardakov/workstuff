#!/bin/bash
ffmpeg -i $1.mp3 -map 0:0 -c:a copy -y -filter_complex "movie=$1.mp4:loop=32, setpts=N/(FRAME_RATE*TB)" $1.loop.mp4
