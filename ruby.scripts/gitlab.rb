#!/usr/bin/env ruby
require 'net/http'

uri = URI('https://gitlab.com')
interval = 15 # in seconds
runtime  = 1  # in minutes

probes = []
tm_init = Time.now
fails = 0
runtime *= 60

loop do
  begin
    tm_start = Time.now
    res = Net::HTTP.get_response uri
    msg = "#{res.code} #{res.message}"
    tm_elapsed = (Time.now - tm_start) * 1000

    puts "OK: #{tm_elapsed.round 3} ms | #{msg}"
    probes.push tm_elapsed
  rescue StandardError => err
    puts "FAIL: #{err.message}"
    fails += 1
  end
  break if Time.now - tm_init > runtime
  sleep interval
end

if probes.count.zero?
  puts '100% of tests failed. Check your network connection'
else
  all = probes.count + fails
  tm_avg = (probes.reduce(0, :+) / probes.count).round
  puts "Probes: #{all} failures: #{fails} | Average response time: #{tm_avg} ms"
end
