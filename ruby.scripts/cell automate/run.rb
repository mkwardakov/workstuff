#coding: utf-8
Length = 160
rc = Random.new(Time.now.to_i)

#rule set
Rule = "%08b" % 22
puts Rule
rules = Hash.new
7.downto(0).each { |j|
	pattern = "%03b" % j
	result = Rule[7-j]
	rules[pattern] = result
}
puts rules

#initial fill
generation = Array.new
1.upto(Length).each { |j|
	generation[j] = rc.rand(2).to_s
}

# iterations
loop do
	state = Array.new(generation)
	puts state.join.gsub("0", " ").gsub("1", "#")
	state[0] = state[Length]
	state[Length+1] = state[1]
	#puts state
	1.upto(Length).each { |j|
		ancestors = [state[j-1], state[j], state[j+1]].join
		#print "#{j}:#{state.join} "
		generation[j] = rules[ancestors]
		#sleep(1)
	}
	( 0 == generation.join.to_i ) && break
	sleep(0.05)
end
