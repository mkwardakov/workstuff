require 'rubygems'
require 'mechanize'

a = Mechanize.new
a.agent.http.verify_mode = OpenSSL::SSL::VERIFY_NONE

USERNAME, PASSWORD = '%username%', '******'

page = a.get('https://leprosorium.ru/login') do |page|
  search_result = page.form_with(:id => 'js-auth_form') do |form|
    form.username = USERNAME
    form.password = PASSWORD
  end.submit
end

post_id = 2066379
url = "https://leprosorium.ru/comments/#{post_id}/"

a.get(url) do |page|
	comments = page.search('.c_i')
	comments.each do |comment|
		# тут текст коммента
		body = comment.at('.c_body').text.strip
		# тут автор коммента
		name = comment.at('.c_user').text.strip
	end
end
