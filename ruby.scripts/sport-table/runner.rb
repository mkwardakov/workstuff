# encoding: utf-8
# after implementing, you may want to run: ruby -run -e httpd ./ -p 8080
# from the script folder and see the result at http://localhost:8080/scoretable.html
require 'nokogiri'
require 'open-uri'
require 'json'
require 'iconv'
# require 'mongo'
require 'logger'
require_relative './laplassian.rb'

# database_connection_url = 'mongodb://ubuntu:ubuntu@localhost:27017/admin'
# Mongo::Logger.logger.level = ::Logger::WARN
# $database = Mongo::Client.new(database_connection_url)

def update_cached_data(url, file)
  now = Time.now
  if File.exists?(file) && (now - File.mtime(file) < 60 * 30) # update after 30 minutes
    puts "\tCached html page seem to be quite fresh"
    return true
  end
  puts "\tDownload fresh copy of the page"
  html = open(url).read
  html = Iconv.conv('UTF-8', 'Windows-1251', html)
  webdoc = Nokogiri::XML(html)
  webdoc = webdoc.css('.score')
  File.write(file, webdoc)
end

def parse_and_store_data(file)
  # read file and init
  webdoc = Nokogiri::HTML(open(file))
  tour_no, game_no, season = 0, 0, {}
  season.store(:teams, Array.new)
  #processing. each row has either tour header or match result
  webdoc.css('tr').each do |row|
    if row.css('th').empty?
      game_no += 1
      home_team = row.at_css('.right').content.strip
      away_team = row.at_css('.left').content.strip
      date = row.at_css('td').content.strip
      if row.css('span').empty?
        link = ""
        score = "?:?"
      else
        link = 'http://football.sport-express.ru' 
        link += row.at_css('span a').nil? ? "" : row.at_css('span a')["href"]
        score = row.at_css('span').content.strip.gsub(' ', '')
      end
      # puts "Game #{game_no} on #{date}. #{home_team}-#{away_team} #{score}"
      match = {
        :home_team => home_team,
        :away_team => away_team,
        :score     => score,
        :date      => date,
        :link      => link,
        :tour      => tour_no
      }
      season.store(game_no, match)
      i = { away_team => game_no }
      if !season.has_key?(home_team) # 1 == tour_no
        season.store(home_team, Hash.new)
        season[:teams].push(home_team)
      end
      season[home_team].merge!(i)
    else
      tour_no += 1
    end
    print "."
  end
  season.store(:matches, game_no)
  season.store(:tours, tour_no)
  puts "\n\t#{tour_no} tours, #{game_no} matches"
  return season
end

def format_data_to_html(data, league_name)
  teams = data[:places]
  teams_total = teams.count
  webdoc = Nokogiri::HTML::Builder.new(:encoding => 'UTF-8') do |doc|
    doc.html {
      # add some css
      css_styles = <<-END
        table{border-width:0px;background-color:black;border-spacing:1px;font-family:sans-serif;table-layout:fixed;}
        th{padding:3px;background-color:wheat;text-align:left;font-size:small;font-weight:bold;}
        td{padding:5px;text-align:center;font-weight:bold;}
        .won{background-color:greenyellow;}
        .drawn{background-color:lightblue;}
        .lost{background-color:tomato;}
        .future{background-color:ghostwhite;color:lightgray;}
        .nogame{background-color:gray;}
        .stat{background-color:lightyellow;text-align:right;padding-right:10px;font-weight:bold;}
        a{font-variant:bold;color:black;text-decoration:none;}
      END
      doc.head { doc.style('type' => 'text/css') { doc.text css_styles } }

      doc.body() {
        doc.h1 { doc.text "Сезон #{league_name} #{SEASON}: сыграно #{data[:played]} из #{data[:matches]} игр" }
        doc.table {
          # column headers
          doc.tr {
            doc.th { doc.text '#'}
            doc.th { doc.text ''}
            teams_total.times { |t| doc.th { doc.text teams[t].slice(0,3) } } # away teams
            %w(Игр Побед Ничьих Поражений Голы Разница Очков).each { |h| doc.th { doc.text h } }
          }
          # print rows
          teams_total.times do |j|
            doc.tr {
              host_team = teams[j]
              host_team_data = data[host_team]

              doc.th { doc.text j + 1 }
              doc.th { doc.text host_team } # host team name

              teams_total.times do |k| # print out games played home
                # get game data
                match_no = host_team_data[teams[k]]
                match = data[match_no]

                # no game with themselves or no game scheduled - just leave  blank
                if j == k
                  doc.td('class' => 'nogame') { doc.text "" }
                  next
                elsif !host_team_data.has_key?(teams[k])
                  doc.td('class' => 'nogame') { doc.text "N/Arr" }
                  next
                elsif match.nil?
                  doc.td('class' => 'nogame') { doc.text "N/MiB" }
                  next
                end

                # match is in the future?
                if  match[:link] == ""
                  # text = "#{match[:tour]} on #{match[:date]}"
                  doc.td('class' => 'future', 'title' => match[:date]) { doc.text "#{match[:tour]} тур" }
                  next
                end

                # match is played
                text = match[:score]
                ex = eval(text.gsub(/:/,"-"))
                csscl = case # highlight match score
                        when ex > 0 then 'won'
                        when ex < 0 then 'lost'
                        when ex == 0 then 'drawn'
                        end
                doc.td('class' => csscl) { doc.a('href' => match[:link]) { doc.text text } }
              end

              # print out totals
              [:games, :won, :drawn, :lost, :goals, :gdiff, :points].each { |j| doc.td('class' => 'stat') { doc.text data[host_team][j] } }
            }
          end
        }
      }
      doc.h1 { doc.text "Альтернативная таблица взвешенных результатов" }
      doc.table {
          alt_stand = data[:alt_scores].sort_by(&:last).reverse
        # column headers
        doc.tr {
          %w(# Команда).each { |h| doc.th { doc.text "#{h}" } }
          alt_stand.each { |h| doc.th { doc.text "#{h[0].slice(0,3)}" } }
          %w(Потенциал Результаты).each { |h| doc.th { doc.text "#{h}" } }
        }
        teams_total.times do |j|
          doc.tr {
            altteams = []
            alt_stand.each { |t| altteams.push(t[0]) }
            doc.th { doc.text "#{j + 1}" }
            doc.th { doc.text "#{altteams[j]}" }
            tmj = data[:teams].index(altteams[j])
            teams_total.times do |k|
              tmk = data[:teams].index(altteams[k])
              if j == k
                doc.td('class' => 'nogame') { doc.text "" }
                next
              end
              ex = data[:alt_results][tmj][tmk] - data[:alt_results][tmk][tmj]
              csscl = case # highlight match score
                      when ex > 0 then 'won'
                      when ex < 0 then 'lost'
                      when ex == 0 then 'drawn'
                      end
              doc.td('class' => csscl) { doc.text "#{data[:alt_results][tmj][tmk].round(2)}" }
            end
            doc.td('class' => 'stat') { doc.text "#{data[:potentials][altteams[j]]}" }
            doc.td('class' => 'stat') { doc.text "#{alt_stand[j][1]}" }
          }
        end
      }
    }
  end
  return webdoc
end

def analyze_and_sort(season)
  season.store('table', Hash.new)
  stats = {'games': 0, 'won': 0, 'drawn': 0, 'lost': 0, 'goals': 0, 'gdiff': 0, 'points': 0, 'place': 0}

  # add stats to each team
  season[:teams].each do |t|
    season[t].merge!(stats)
    season[t].merge!(:id => t)
  end

  played_count = 0
  # process each match
  season[:matches].times do |m|
    match = season[m + 1]
    "" == match[:link] && next # match not yet played
    played_count += 1
    score = match[:score]
    ex = eval(score.gsub(/:/,"-"))
    gls1, gls2 = score.split(':')
    team1 = match[:home_team]
    team2 = match[:away_team]
    season[team1][:games] += 1
    season[team2][:games] += 1
    season[team1][:goals] += gls1.to_i
    season[team2][:goals] += gls2.to_i
    case
    when ex > 0 # host team won
      season[team1][:won] += 1
      season[team2][:lost] += 1
    when ex == 0 # drawn
      season[team1][:drawn] += 1
      season[team2][:drawn] += 1
    when ex < 0 # host team lost
      season[team1][:lost] += 1
      season[team2][:won] += 1
    end
    season[team1][:gdiff] += ex
    season[team2][:gdiff] -= ex
    print "."
  end

  # calc points
  season[:teams].each do |t|
    team = season[t]
    team[:points] = team[:won] * 3 + team[:drawn]
  end

  # sort and arrange places
  arranged, places = [], []
  season[:teams].each { |t| arranged.push(season[t]) }
  arranged.sort_by! { |t| t[:points] }
  arranged.reverse!.each { |t| places.push(t[:id]) }
  season.store(:places, places)
  season.store(:played, played_count)

  # TBD: look to 2 upcoming tours and stash matches between leaders (1-5 places)

  puts "\n\t#{played_count} played matches analyzed and put to table"
  return season
end

def calculate_laplassian(season)
  # init results matrix
  results = []
  teams_no = season[:teams].length
  teams_no.times.each do |j|
    sub = []
    teams_no.times.each { |k| sub.push(0.0)}
    results.push(sub)
  end
  # count results
  season[:matches].times do |m|
    match = season[m + 1]
    "" == match[:link] && next # match not yet played
    score = match[:score]
    gls1, gls2 = score.split(':')
    gls1 = gls1.to_f
    gls2 = gls2.to_f
    if gls1 < gls2
        away_score = 3.0
        home_score = 0.0
    elsif gls1 == gls2
        home_score = 1.34
        away_score = 1.66
    else
        home_score = 2.5
        away_score = 0.5
    end
    home_score += gls1/2 #(gls1 - gls2)/3 + 1.5
    away_score += gls2/2 #(gls2*1.3 - gls1)/3 + 2
    team1 = season[:teams].index(match[:home_team])
    team2 = season[:teams].index(match[:away_team])
    results[team1][team2] += home_score
    results[team2][team1] += away_score
    print "."
  end
  # calculate
  mkirch = get_kirchhoff(results)
  potentials = []
  scores = []
  puts "\n\tMatches analyzed and put to table"
  teams_no.times.each do |j|
    potentials.push( get_determinant( get_minor( mkirch, j ) ) )
    print "."
  end
  puts "\n\tPotentials have been calculated"
  normalize(potentials)
  teams_no.times.each do |j|
    score = 0
    teams_no.times.each { |k| score += potentials[k]*results[j][k]}
    scores.push(score.round(2))
  end
  puts "\n\tScores have been calculated"
  # assign data to each team
  named_potentials = {}
  named_scores = {}
  teams_no.times.each do |j|
    named_potentials.store(season[:teams][j], potentials[j])
    named_scores.store(season[:teams][j], scores[j])
  end
  season.store(:alt_results, results)
  season.store(:potentials, named_potentials)
  season.store(:alt_scores, named_scores)
  return season
end

SEASON = '2015-2016'.freeze
BASEURL = 'http://football.sport-express.ru'.freeze
ROOTFILE = 'index.html'.freeze

leagues = [
  {:name => 'Премьер лига', :url => "#{BASEURL}/russia/premier/#{SEASON}/calendar/tours/", :id => 'ru'},
  {:name => 'ФНЛ', :url => "#{BASEURL}/russia/first/#{SEASON}/calendar/tours/", :id => 'ru2'},
  {:name => 'Premier League', :url => "#{BASEURL}/foreign/england/premier/#{SEASON}/calendar/tours/", :id => 'en'},
  {:name => 'Champion League', :url => "#{BASEURL}/foreign/england/champion/#{SEASON}/calendar/tours/", :id => 'en2'},
  {:name => 'Primera La League', :url => "#{BASEURL}/foreign/spain/laleague/#{SEASON}/calendar/tours/", :id => 'es'},
  {:name => 'Segunda La League', :url => "#{BASEURL}/foreign/spain/segunda/#{SEASON}/calendar/tours/", :id => 'es2'},
  {:name => 'Seria A', :url => "#{BASEURL}/foreign/italy/seriaa/#{SEASON}/calendar/tours/", :id => 'it'},
  {:name => 'Seria B', :url => "#{BASEURL}/foreign/italy/seriab/#{SEASON}/calendar/tours/", :id => 'it2'},
  {:name => 'Bundesliga 1', :url => "#{BASEURL}/foreign/german/bundes1/#{SEASON}/calendar/tours/", :id => 'de'},
  {:name => 'Bundesliga 2', :url => "#{BASEURL}/foreign/german/bundes2/#{SEASON}/calendar/tours/", :id => 'de2'},
#   {:name => 'League 1', :url => "#{BASEURL}/foreign/france/league1/#{SEASON}/calendar/tours/", :id => 'fr'},
  {:name => 'Eredividie', :url => "#{BASEURL}/foreign/netherlands/highest/#{SEASON}/calendar/tours/", :id => 'ne'},
  {:name => 'Primeira Liga', :url => "#{BASEURL}/foreign/portugal/super/#{SEASON}/calendar/tours/", :id => 'po'},
  {:name => 'Премьер лига', :url => "#{BASEURL}/foreign/ukraine/premier/#{SEASON}/calendar/tours/", :id => 'ua'}
]

rootdoc = Nokogiri::HTML::Builder.new(:encoding => 'UTF-8') do |doc|
  doc.html {
    # add some css
    css_styles = <<-END
      body{font-family:sans-serif;}
      h1{font-weight:bold;font-size:16px;}
      ul{}
      li{font-size:14px;margin:0.75em 0 0 5em}
    END
    doc.head { doc.style('type' => 'text/css') { doc.text css_styles } }

    doc.body() {
      doc.h1 { doc.text "Таблицы футбольных чемпионатов сезона #{SEASON}" }
      doc.ul {
        leagues.each do |ch|
          puts "\n\n\n\n\n***** #{ch[:name]} *****"
          puts "Update match result source"
          cache_file = "./index-#{ch[:id]}.html"
          update_cached_data(ch[:url], cache_file)

          print "Parse match data"
          season_data = parse_and_store_data(cache_file)

          print "Analyze data"
          season_data = analyze_and_sort(season_data)

          print "Calculate laplassian stands"
          season_data = calculate_laplassian(season_data)

          puts "Format data to html and write it to a file"
          html_table = format_data_to_html(season_data, ch[:name])
          File.write("./data-#{ch[:id]}.json", season_data.to_json)
          File.write("./table-#{ch[:id]}.html", html_table.to_html)
          doc.li { doc.a(:href => "table-#{ch[:id]}.html") { doc.text "Лига: #{ch[:name]}" } }
        end
      }
    }
  }
end

File.write(ROOTFILE, rootdoc.to_html)
