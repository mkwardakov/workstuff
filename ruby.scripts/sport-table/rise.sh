#!/bin/bash
WSCONT='st.nginx'
WSDIR='/opt/st.www'
DBCONT='st.mongo'
DBDIR='/opt/st.db'
user=$(whoami)

if [[ -d $DBDIR ]]; then
    echo "$DBDIR exists"
else
    sudo mkdir -v $DBDIR
    sudo chown -v $user:$user $DBDIR
fi

if [[ -d $WSDIR ]]; then
    echo "$WSDIR exists"
else
    sudo mkdir -v $WSDIR
    sudo chown -v $user:$user $WSDIR
fi

if [[ $(docker ps | grep $WSCONT) ]]; then
    echo "Mongo container is already running"
else
    if [[ $(docker ps -a| grep $WSCONT) ]]; then
        docker start $WSCONT
    else
        docker run --name $WSCONT -p 80:80 -v $WSDIR:/usr/share/nginx/html -d nginx
    fi
fi

if [[ $(docker ps | grep $DBCONT) ]]; then
    echo "Mongo container is already running"
else
    if [[ $(docker ps -a| grep $DBCONT) ]]; then
        docker start $DBCONT
    else
        docker run --name $DBCONT -p 27017:27017 -v $DBDIR:/data/db -d mongo
        # docker exec -it st.mongo mongo admin
        # db.createUser({ user: 'ubuntu', pwd: 'ubuntu', roles: [ { role: "userAdminAnyDatabase", db: "admin" } ] });
    fi
fi
