def get_kirchhoff(matrix)
  kirchhoff = []
  sz = matrix.length
  sz.times.each do |j|
    sum = 0
    sub = []
    sz.times.each do |k|
      sum += matrix[k][j]
      sub.push(-1 * matrix[k][j])
    end
    kirchhoff.push(sub)
    kirchhoff[j][j] = sum
  end
  return kirchhoff
end

def get_minor(matrix, index)
  minor = []
  sz = matrix.length
  sz.times.each do |j|
    next if j == index
    subm = []
    sz.times.each { |k| subm.push(matrix[j][k]) unless k == index }
    minor.push(subm)
  end
  return minor
end

def get_gaussian(matrix)
  sz = matrix.length - 1
  gaussian = []
  matrix.each { |sub| gaussian.push(sub) }
  # Gaussian elimination
  1.upto(sz).each do |j|
    j.upto(sz).each do |k|
      coeff = gaussian[k][j-1].to_f/gaussian[j-1][j-1]
      0.upto(sz).each { |m| gaussian[k][m] = gaussian[k][m] - coeff*gaussian[j-1][m] }
    end
  end
  return gaussian
end

def get_determinant(matrix)
  sz = matrix.length
  if sz <= 2 then
    determinant = matrix[0][0]*matrix[1][1] - matrix[0][1]*matrix[1][0]
  else
    determinant = 1
    mgauss = get_gaussian(matrix)
    sz.times.each { |j| determinant *= mgauss[j][j] }
  end
  return determinant
end

def normalize(arr)
  min = arr.sort.shift
  arr.length.times.each { |j| arr[j] = (arr[j]/min*10).round(2)}
end
