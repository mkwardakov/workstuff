# while true; do inotifywait -q -e close_write /home/user/FLD/ && ls *.html | wc -l && du -hs; done
# wget -v -nc -nd -r -l 100 -p -np --reject-regex "^.*\?.+" --regex-type posix http://vorath.livejournal.com/
# Bad posts: 186907 
# encoding: UTF-8
require 'rubygems'
require 'nokogiri'
require 'base64'

#require_relative 'lib/fb2builder.rb'
#require_relative 'lib/htmlparser.rb'

options = "" and ARGV.each {|arg| options += arg }
wgetSummon = 'wget --execute robots=off --no-clobber --no-directories --recursive --no-parent --level=100 --reject-regex "^.*\?.+" --regex-type posix'
address = options[/http:\/\/.*\.livejournal.com/]
address.nil? or wgetResult = `#{wgetSummon} #{address}`
isVerbose = ! options[/-v/].nil?
#isCover = 

files = Dir.glob("*.html")
files.each {|f|
	case f.length
		when 8 
			File.rename(f, "000"+f)
		when 9
			File.rename(f, "00"+f)
		when 10
			File.rename(f, "0"+f)
	end
}

files = Dir.glob("*.html").sort!
#cover = "cover.png.txt"
result = "book.fb2"

puts "#{files.length} posts in total"

data = []
skipped = 0
#files.each { |j|
(0..3).each { |cnt|
	currentFile = files[cnt]
	doc = Nokogiri::HTML(open(currentFile, &:read)) { |cfg| cfg.noerror.noblanks }
	if !doc.at_css(".entry-content").nil? ; then
		profile = {content: "div.entry-content", tags: "div.ljtags", header: "h4", date: "entry-author-date"}
	else
		profile = {content: "div.asset-body", tags: "div.asset-name", header: "div.asset-tags-list", date: "entry-author-date"}
	end
	story = doc.at_css(profile[:content])
	if story.nil?
		is Verbose or puts "No conent found in #{currentFile}; skipping."
	end
	header = doc.at_css(profile[:header])
	header = header.nil? ? "": header.content
	tags = doc.at_css(profile[:tags])
	tags = tags.nil? ? "": tags.content
	date = doc.at_css(profile[:date])
	date = date.nil? ? File.mtime(currentFile) : date.content
	["a[name='cutid1']", "a[name='cutid1-end']","div.user-icon", "form", "div.lj-like", "iframe", "img"].each {|test|
		story.css(test).each {|t| t.remove}
	}
	story.css("span").each {|t|
		t.swap(t.inner_html)
	}
	story.css("a").each {|a|
		a.text.empty? and a.remove
	}
	entry = {
		hdr: header,
		cnt: story.inner_html,
		tgs: tags,
		lnk: "http://vorath.livejournal.com/"+currentFile,
		dat: date
	}
	data.push(entry)
}
puts "#{skipped} posts skipped"
book = Nokogiri::XML::Builder.new(:encoding => 'UTF-8') do |xml|
	xml.FictionBook(:"xmlns:l" => "http://www.w3.org/1999/xlink", :"xmlns" => "http://www.gribuser.ru/xml/fictionbook/2.2") {
		xml.description {
			xml.genre	"magician_book"
			xml.author {
				xml.send(:"first-name", "Владислав")
				xml.send(:"last-name", "Силин")
				xml.send(:"nickname", "Vorath")
			}
			xml.send(:"book-title", "Чайный домик Фу Ла Дэ")
			xml.annotation	"Если на клетке буйвола видишь табличку \"Зверь по имени кот\" - не верь глазам своим"
			xml.date		"2013-05-16"
			#xml.coverpage	"cover.png"
			xml.lang		"ru"
			xml.send(:"src-lang", "ru")
			xml.send(:"document-info") {
				xml.author {
					xml.send(:"first-name", "")
					xml.send(:"last-name", "")
					xml.email		"kwardakov@gmail.com"
				}
				xml.send(:"program-used", "A ruby script")
				xml.date "2015-07-21"
			}
		}
		xml.body {
			data.each do |j|
				xml.section {
					isVerbose and print "."
					xml.title		"#{j[:hdr]} (#{j[:lnk]})"
					xml.epigraph	j[:tgs]
					j[:cnt].gsub!("- ", "— ")
					paras = j[:cnt].split("<br>")
					paras.each do |p|
						#p.empty? and next
						xml.p p
					end
				}
			end
		}
		#xml.binary(open(cover, &:read), :"content-type" => "image/png", :"id" => "cover.png")
	}
end
isVerbose and puts

book = book.to_xml
book.gsub!("&lt;", "<").gsub!("&gt;", ">").gsub!("&amp;", "&")

fout = File.new(result, "w")
fout.write(book)
fout.close
