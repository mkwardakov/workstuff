#coding: utf-8
require 'open-uri'
require 'nokogiri'
require 'time'

puts "What type of rhymes you'd like to choose?"
vtype = gets.chomp
verses = Array.new

j = 1
puts "Fetching verse #:"
loop do
	begin
		doc = Nokogiri::HTML(open("http://autopoet.yandex.ru/#{vtype}/#{j}/"))
		verses[j*2-1] = "<h4>#{j}</h4>" + doc.css('div.poem__text').to_s
		verses[j*2] = doc.css('p.poem__year').to_s + "<hr/>"
	rescue URI::InvalidURIError, OpenURI::HTTPError => error
		puts "\n#{error}"
		puts "Filed #{j} verses so far"
		break
	end
	j += 1
	print "\r#{j}"
end

puts

tm = Time.now.strftime "%d.%m.%Y at %H:%M:%S"
timestamp = "Generated #{tm}"

File.open("AutoPoet-#{vtype}.html", "w") { |file| 
	file.write "<!DOCTYPE HTML><html><head><meta charset='utf-8'/><title>Яндекс.Автопоэт.#{vtype}</title>"
	file.write "<style type='text/css'>div.poem__text {font-family:verdana;font-size:15px}</style></head><body>"
	verses.each { |item|
		file.write item
	}
	file.write "<div>#{timestamp}</div></body></html>"
}
