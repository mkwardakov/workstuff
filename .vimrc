set shortmess=I                 " Do not show intro on start
" General options
set nocompatible                " Forget Vi
set undolevels=300              " How many undos
set history=100                 " Command history
set encoding=utf8               " Use encoding to display

" File options
set autoread                    " Set to auto read when a file is changed from the outside
set nobackup                    " Turn backup off, since most stuff is in SVN, git
set nowritebackup               " Do not make backup when overwriting a file
set noswapfile                  " Do not use a swapfile for the buffer

" Display options
set wildmenu                    " Turn on the WiLd menu
set laststatus=2                " Always show status line
set lazyredraw                  " Don't redraw while executing macros (good performance config)
set cmdheight=2                 " Height of the command bar
set statusline=%r               " Read only flag
set statusline+=%m              " Modified flag
set statusline+=%c.             " Cursor column
set statusline+=%l/%L           " Cursor line/total lines
set statusline+=%=              " Left/right separator
set statusline+=\ %P            " Percent through file
set statusline+=\ of\ %F        " Full filename
set statusline+=%y              " Filetype
set ruler                       " Always show current position
set number                      " Show line numbers

" netrw explorer options
let g:netrw_banner=0            " No banner
let g:netrw_liststyle=3         " Show file tree in the :E netrw explorer
let g:netrw_browse_split=0      " Show netrw in vertical split

" Code formatting options
syntax enable
set wrap                        " Wrap lines
set textwidth=0 wrapmargin=0    " Turn off physical line wrapping
set expandtab                   " Do not replace spaces with tabs
set smarttab                    " Insert blanks in front of a line on <Tab>
set tabstop=4                   " 4 spaces makes 1 <Tab>
set shiftwidth=4                " Shift to 4 spaces
set showmatch                   " When a bracket is inserted, briefly jump to the matching one
set matchtime=3                 " How many tenths of a second to blink when matching brackets
set scrolloff=2                 " How many lines show below and above current
set list!                       " Show some unprintable chars:
set listchars=tab:→·,trail:·    " Tabs and trailings spaces

" Search options
set magic                       " For regular expressions turn magic on
set ignorecase                  " Ignore case when searching
set smartcase                   " When searching try to be smart about cases
set hlsearch                    " Highlight search results
set incsearch                   " Makes search act like search in modern browsers

" Macros
runtime macros/matchit.vim

" Hotkey settings 
set backspace=eol,start,indent  " Configure backspace so it acts as it should act
set pastetoggle=<F10>           " Toggle paste option
"   Show all buffers
nnoremap <F2> :ls<CR>
"   Show what is changed before last save
nnoremap <F3> :w !diff - %<CR>
"   Turn off search highlight
nnoremap <F5> :nohlsearch<CR>
"   Toggle on/off windows scroll bind
nnoremap <F6> :set scrollbind!<CR>
"   Bind new tab
nnoremap <C-t> :tabnew<CR>
"   Quick exit
nnoremap <C-x> :q!<CR>
nnoremap <C-s> :w<CR>
nnoremap <C-Up> <C-y>
nnoremap <C-Down> <C-e>

" Commands case fix
command WQ wq
command Wq wq
command W w
command Q q

" Behavior options
set autoindent                  " Copy indent from current line when starting a new line
set smartindent                 " Do smart autoindenting when starting a new line, also: cindent
set showfulltag                 " Show full tags when doing completion
set whichwrap+=<,>,h,l          " Allow cursor move to next/previous line

" Vundle and plugins
" git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim
filetype off
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
Plugin 'VundleVim/Vundle.vim'           " The plugin manager
Plugin 'vim-airline/vim-airline'        " Lean & mean status/tabline for vim that's light as air
Plugin 'vim-airline/vim-airline-themes' "
Plugin 'vim-ruby/vim-ruby'              " Contains configuration files for editing and compiling Ruby
Plugin 'elzr/vim-json'                  " Distinct highlighting of keywords vs values, JSON-specific (non-JS) warnings, quote concealing
Plugin 'vim-scripts/python_match.vim'   " Redefines the % motion so that (in addition to its usual behavior) it cycles through if/elif/else, try/except/catch etc
Plugin 'vim-scripts/EnhCommentify.vim'  " Comments made easily: <Leader>+x
Plugin 'tpope/vim-sleuth'               " This plugin automatically adjusts 'shiftwidth' and 'expandtab' heuristically based on the current file
Plugin 'tpope/vim-endwise'              " Helps to end certain structures automatically
Plugin 'tpope/vim-surround'             " All about surroundings: parentheses, brackets, quotes, XML tags, and more
Plugin 'tpope/vim-fugitive'             " Best Git wrapper of all time. :Gstatus, :Gcommit, :Gblame, :Glog
Plugin 'tpope/vim-repeat'               " Remaps . in a way that plugins can tap into it
Plugin 'airblade/vim-gitgutter'         " Shows a git diff in the 'gutter' (sign column)
Plugin 'Raimondi/delimitMate'           " Provides automatic closing of quotes, parenthesis, brackets, etc.
Plugin 'pearofducks/ansible-vim'        " Syntax plugin for Ansible 2.0, it supports YAML playbooks, Jinja2 templates, and Ansible's hosts files
Plugin 'Shougo/neocomplete.vim'         " Provides keyword completion system by maintaining a cache of keywords in the current buffer
Plugin 'scrooloose/syntastic'           " Advanced syntax checker
Plugin 'mattn/gist-vim'                 " Send code to gist.github.com
Plugin 'vim-ctrlspace/vim-ctrlspace'    " tabs / buffers / files management
Plugin 'matze/vim-move'                 " Move lines easily (C-j, C-k)
Plugin 'easymotion/vim-easymotion'      " EasyMotion allows to press one key to jump directly to the target. <Leader> = \
Plugin 'PProvost/vim-ps1.git'           " PowerShell syntax highlighter
" Colors
Plugin 'alessandroyorba/despacio'
"Plugin 'YorickPeterse/happy_hacking.vim'
"Plugin 'fcpg/vim-fahrenheit'
"Plugin 'jamiewilson/predawn'

call vundle#end()
filetype plugin indent on

" Easymotion
nmap <Leader>f <Plug>(easymotion-overwin-f)
nmap <Leader>w <Plug>(easymotion-overwin-w)
nmap <Leader>l <Plug>(easymotion-overwin-line)
nmap s <Plug>(easymotion-overwin-f2)
" Airline
let g:airline#extensions#tabline#enabled=1
let g:airline_detect_modified=1
let g:airline_theme='hybrid'
" vim-move
let g:move_key_modifier = 'C'
" Required by ctrlspace
set nocompatible
set hidden
" Syntastic (pip install ansible-lint; gem install rubocop)
" :SyntasticCheck :SyntasticInfo :SyntasticReset :lclose :lnext :lprevious
nnoremap <F9> :SyntasticCheck<CR>
nnoremap <C-F9> :Errors<CR>

set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*

let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 0
let g:syntastic_check_on_wq = 0
let g:syntastic_loc_list_height = 5
let g:syntastic_mode_map = { "mode": "passive", "active_filetypes": [], "passive_filetypes": [] }

let g:syntastic_ruby_checkers = ['mri', 'rubocop']
let g:syntastic_ansible_checkers = ['ansible-lint']

let g:gist_detect_filetype = 1

" Custom functions here
" DiffWithSaved shows diff between current version and saved recently
function! s:DiffWithSaved()
  let filetype=&ft
  diffthis
  vnew | r # | normal! 1Gdd
  diffthis
  exe "setlocal bt=nofile bh=wipe nobl noswf ro ft=" . filetype
endfunction
com! Diff call s:DiffWithSaved()

set indentexpr=
set showcmd                     " Show a command as you type
