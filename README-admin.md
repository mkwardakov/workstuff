# Administrating Linux

## Everyday tasks 

- `maybe` sandbox; allows to run a command and see what it does to your files
- `cat /proc/29735/cmdline` show the command line for a PID
- `at now+10m notify-send -t 10000 "header" "message"` simple reminder
- `kill -9 $$` quit session without saving history
- `script -q -t $FOLDER$USER.$DATE 2> $FOLDER$USER.$DATE.time; scriptreplay test.2010-03-25.20.01.time test.2010-03-25.20.01` record a terminal session and then replay it. Also: `ttyrec`
- Remember `screen`, `tmux` и `dtach`
- Use `banner` or `figlet` when sending system announce
- `env -i bash -x -l -c 'echo 123' >login.log 2>&1` shows full log for shell init
- To test DNS over TLS (DoT) cannot use host or dig CLI. Instead: `kdig -d @185.49.141.38 +tls-ca +tls-host=getdnsapi.net ya.ru`. `knot-utils` package required

## Web services

- `ruby -run -e httpd -- -p 5000 .` web server from current folder in ruby. For python use `python -m SimpleHTTPServer`
- `curl --basic --user USERNAME:PASSWORD --data status="$*" 'http://twitter.com/statuses/update.xml' -o /dev/null` Send result of last command to twitter
- `curl ipinfo.io` Show geolocation data on your external ip 
- `xargs -L1 -P4 wget -q < url.list` download URL list in 5 streams
- `wrk` [modern HTTP benchmarking tool](https://github.com/wg/wrk)
- `openssl s_client -servername www.nixcraft.com -connect www.nixcraft.com:443 | openssl x509 -noout -dates` check TLS certs

## Network

- `dig +short myip.opendns.com @resolver1.opendns.com` know your public ip address
- `mtr google.com` traceroute + ping
- `ss -nltcp` quickly list all TCP/UDP port on the server (`netstat` is deprecated)
- `lsof -i TCP:80` show processes using port *80* either locally or remotely. Need to be root for unowned processes
- `lsof -aP -i TCP -p $(cat /var/run/tomcat.pid)` show all ports used by tomcat
- `ngrep` grep a network layer
- Forget `nslookup`, use `host` and `dig`
- Check if the port is open or not:
  - `telnet localhost 80` 
  - `echo 1 > /dev/tcp/localhost/2181 ; echo $?`
  - `nc -zv localhost 80` 
- `for i in {1..254}; do host 192.168.0.$i; done` Do a reverse DNS lookup of all hosts
- `netstat -n | tee /dev/stderr | wc -l` Summarize established connections after netstat output
- [nettop](https://github.com/Emanem/nettop): utility to show network traffic split by process and remote host
- `bmon`: bandwidth monitor and rate estimator
- `iftop`: display bandwidth usage on an interface by host
- `iptraf-ng`: ncurses-based IP LAN monitor that generates various network statistics
- `socat -v UDP-LISTEN:8161,fork TCP:localhost:8161` encapsulate UDP packets in a TCP stream so it can be tunneled through an SSH tunnel, etc

## File operations

- `pycp` Copy with progress bar
- `mkdir -p {0..255}/{0..255}` Generate 256 directories with 256 subdirectories inside each
- `find / -dev -size +3000 -exec ls -l {} ;` Find large files across filesystems
- `truncate` or `fallocate` (ext4, xfs, btrfs and ocfs2) or `xfs_mkfile` or `mkfile`(MacOS) to create big dummy files really fast
  - `fallocate -l 32GB /tmp/dummy32GBfile`
- `rsync -nav -e ssh /etc root@remotehost:/etc` Just see what files under **/etc** are different between the localhost and remotehost
- `rsync -avxHAXW --info=progress2 /old-disk /new-disk/` Copy the contents of one partition to another
- `gzip -c large.log | ssh user@hostwithbigdisk 'cat > /dir/large.log.gz'` Compress and ssh transfer on the fly a log that has filled a filesystem
- `touch /forcefsck` to force a fs check on next boot
- `findmnt` show all mounts

# System tasks

- Profiling: 
  - `lsof` just lists open files
  - `dstat` fancy io monitoring
  - `htop` smart `top` replacement; see `nmon` as well
  - `glances` even smarter `top` replacement
  - `iostat` shows io statistics
  - `last` shows a listing of last logged in users
  - `sar` collects and reports system activity information
  - `ss` utility to investigate sockets
  - `ccze` for beautiful logs

## SSH

- `ssh-copy-id hostname` Copy pubkey over to remote host easily

The ssh command has to be executed on the blue computer

Local option
![Local](https://raw.githubusercontent.com/mkwardakov/workstuff/master/sshltunnel.png)

Remote option
![Remote](https://raw.githubusercontent.com/mkwardakov/workstuff/master/sshrtunnel.png)

## Observation
![Cheatsheet](https://raw.githubusercontent.com/mkwardakov/workstuff/master/linuxperformanceobservabilitytools.png)

## Package management
- `cat /etc/*-release` or `lsb_release -a` or `lsb-core` or `redhat-lsb` shows distro version
- `diff <(ssh server1 'rpm -qa | sort') <(ssh server2 'rpm -qa | sort')` See what packages are missing
- `rpm -qa package` или `dpkg-query -L package` Display list of all installed packages
- `rpm -qf /etc/passwd` Find out what package a file belongs to
- `rpm -qcf /usr/X11R6/bin/xeyes` Display list of configuration files for a command
- `rpm -qpR mediawiki-1.4rc1-4.i586.rpm` и `rpm -qR bash` Find out what dependencies a rpm file has

## Security
- [SQL injection](http://sqlmap.org/)
- [Web Application testing tool](https://portswigger.net/burp/)
- *Gmail*, *Outlook.com*, and *iCloud* намеренно игнорируют точку, знак «плюс» и всё, что после него, в email. *Yahoo* с той же целью «не замечает» дефис. Удобно для защиты от спама и для тестирования
- Search for archived mail without labels *Gmail* `has:nouserlabels -in:Sent -in:Chat -in:Draft -in:Inbox`
