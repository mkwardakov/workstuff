#!/usr/bin/env python2
from datetime import datetime
from faker import Factory
import random
import time
import json
import uuid
import sys
import math


def generateMessage():
    msg = {}
    msg['timestamp'] = datetime.now().isoformat(' ')[:-3]
    msg['level'] = random.choice(['CRITICAL', 'ERROR', 'WARNING', 'INFO', 'DEBUG', 'TRACE'])
    msg['CID'] = str(uuid.uuid4())
    msg['package'] = random.choice(['core', 'providers', 'billing', 'crm', 'site', 'database', 'registry', 'payments'])
    data = {}
    if msg['package'] == 'core':
        msg['message'] = random.choice(['wtf', 'fubar', 'lolwut', 'gtfo', 'ruok', 'fya', 'twss'])
        msg['recovered'] = True if random.random() > 0.3 else False
        msg['amount'] = random.randint(1, 1000)
    elif msg['package'] == 'providers':
        data['company'] = fake.company()
        data['resident'] = fake.address()
        msg['provider'] = data
    elif msg['package'] == 'billing':
        data['client'] = fake.name()
        data['check'] = round(random.random() * 1000, 2)
        msg['bill'] = data
    elif msg['package'] == 'crm':
        data['name'] = fake.name()
        data['email'] = fake.email()
        data['phone'] = fake.phone_number()
        data['position'] = fake.job()
        msg['user'] = data
    elif msg['package'] == 'site':
        data['user'] = fake.user_name()
        data['url'] = fake.domain_name()
        data['ip'] = fake.ipv4()
        data['agent'] = fake.user_agent()
        msg['accessLog'] = data
    elif msg['package'] == 'database':
        msg['operation'] = random.choice(['select', 'alter', 'insert', 'delete'])
        msg['rows'] = random.randint(1, 1000)
        msg['time'] = str(fake.time_delta())
    elif msg['package'] == 'registry':
        data['name'] = fake.file_name()
        data['path'] = fake.file_path()
        msg['file'] = data
    elif msg['package'] == 'payments':
        data['card'] = fake.credit_card_number()
        data['type'] = fake.credit_card_provider()
        data['expired'] = fake.credit_card_expire()
        data['currency'] = fake.currency_code()
        data['amount'] = round(random.random() * 1000, 2)
        msg['paymentInfo'] = data
    return msg

def printUsage():
    print("Usage: json-logger.py [rate] [path]")
    print("\twhere rate should be positive integer > 1")
    print("\tand path is path to folder")

# Handle parameters
rate = 0.4
path = "./"
if len(sys.argv) == 2:
    try:
        rate = 1 / math.log(int(sys.argv[1]), 10)
    except:
        path = sys.argv[1]
elif len(sys.argv) > 2:
    try:
        rate = 1 / math.log(int(sys.argv[1]), 10)
    except:
        printUsage()
    path = sys.argv[2]

try:
    logfile = open(path + "/" + time.strftime("%Y%m%d") + "_jsonl.log", "a", 0)
except:
    print("Cannot open file in path")

fake = Factory.create('ru_RU')

while True:
    message = json.dumps(generateMessage())
    time.sleep(random.random() * rate)
#     print message
    logfile.write(message + "\n")

logfile.close()
