#!/usr/bin/env python3
# This tool splits all mp3 files in current directory into chunks in subdir
import datetime
import os
import glob
import ffmpy
# import re

Chunk = 180  # in seconds
SubDirName = 'splitted'


def getFiles():
    files = glob.glob('*.mp3')
#     r = re.compile('^.')
#     hidden = filter(r.match, files)
    return files


def createSubDir(subdirname):
    if not os.path.exists('./' + subdirname):
        os.mkdir('./' + subdirname)

files = getFiles()
createSubDir(SubDirName)
endparams = " -t " + str(Chunk) + " -c:a copy"

for afile in files:
    start = datetime.timedelta(seconds=0)
    ch = 1
    while True:
        # add chunk number
        outfile = afile[:4] + "-" + ("%02d" % ch) + afile[4:]
        outfile = SubDirName + "/" + outfile
        # set up ffmpeg job
        params = "-ss " + str(start) + endparams
        ff = ffmpy.FFmpeg(inputs={afile: None}, outputs={outfile: params})
        ff.run()
        # if too small, stop. file has been splitted
        if os.stat(outfile)[6] < 10240:
            os.remove(outfile)
            break
        # compute next start point
        start = start + datetime.timedelta(seconds=Chunk)
        ch += 1
