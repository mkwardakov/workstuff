# zsh options
setopt NO_NOMATCH
setopt APPEND_HISTORY
setopt HIST_IGNORE_DUPS
setopt HIST_IGNORE_SPACE
setopt HIST_REDUCE_BLANKS
setopt HIST_SAVE_NO_DUPS
HISTSIZE=16000
HISTORY_IGNORE="*xfreerdp*:grep*"
HYPHEN_INSENSITIVE="true"
SAVEHIST=16000
ZSH_THEME="awesomemy"
CASE_SENSITIVE="true"
COMPLETION_WAITING_DOTS="true"
DISABLE_UNTRACKED_FILES_DIRTY="true"
HISTFILE="$HOME/.zsh_history"
HYPHEN_INSENSITIVE="true"
bindkey '^R'    history-incremental-search-backward
bindkey '^[[1~' beginning-of-line
bindkey '^[[4~' end-of-line
bindkey '\e[A'  history-search-backward
bindkey '\e[B'  history-search-forward

export EDITOR='vim'
export JAVA_HOME='/etc/alternatives/java_sdk_11_openjdk'
export JDK_HOME='/etc/alternatives/java_sdk_11_openjdk'
export LANG=en_US.UTF-8
export LANGUAGE=en_US
export LC_CTYPE=ru_RU.UTF-8
export LC_NUMERIC="ru_RU.UTF-8"
export LC_TIME="ru_RU.UTF-8"
export LC_COLLATE="ru_RU.UTF-8"
export LC_MONETARY="ru_RU.UTF-8"
export LC_MESSAGES="ru_RU.UTF-8"
export LC_PAPER="ru_RU.UTF-8"
export LC_NAME="ru_RU.UTF-8"
export LC_ADDRESS="ru_RU.UTF-8"
export LC_TELEPHONE="ru_RU.UTF-8"
export LC_MEASUREMENT="ru_RU.UTF-8"
export LC_IDENTIFICATION="ru_RU.UTF-8"
export LESS='--clear-screen --status-column --hilite-unread --RAW-CONTROL-CHARS --squeeze-blank-lines --shift=.7 --prompt=?f%f. ?m(file %i of %m \: ).| ?ltlines %lt-%lb?L/%L. | byte %bB?s/%s. | ?pb%pb\%. ?c| column %c.'
export LESSOPEN="| source-highlight -f esc -i %s"
export PATH="$PATH:$HOME/bin:$HOME/.local/bin"
export SSH_KEY_PATH="~/.ssh/id_rsa"
export UPDATE_ZSH_DAYS=30
export XDG_CURRENT_DESKTOP=GNOME

# includes
export ZSH="$HOME/.oh-my-zsh"
eval "$(fasd --init auto)"
source $ZSH/oh-my-zsh.sh
source $HOME/.zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
source $HOME/.fzf.zsh

# ohmyzsh options
plugins=(colored-man-pages colorize command-not-found \
  doctl docker dnf extract fasd fzf git git-extras \
  httpie nmap pip sudo systemd themes tig tmux vscode)

# unalias
unalias l.
unalias la
unalias ll
unalias ls
unalias lsa

# common aliases
alias ....='cd ../../../../'
alias ...='cd ../../../'
alias ..='cd ..'
alias Giants='find ./ -type f -printf "%s\t%p\n" | sort -k1,1n | tail -n 15 | tac'
alias c='cat'
alias cd..='cd ../..'
alias df='df -h'
alias diff='colordiff'
alias du='du -hs'
alias env='env | sort'
alias ip='ip -c'
alias ls='ls --hyperlink --color=always'
alias l='ls -al'
alias lsz='find ./ -printf "%s\t%P\n"'
alias meminfo='free -m -l -t'
alias memps='ps auxf | sort -nr -k 4 | head -10'
alias mount='mount | column -t'
alias now='date +"%T"'
alias path='echo -e ${PATH//:/\\n}'
alias pgrep='pgrep -la'
alias ports='ss -pant'
alias ps='ps auxw'
alias pwgen='pwgen -nyc1 32 10'
alias sduo='sudo'
alias topps='ps auxf | sort -nr -k 3 | head -10'
alias vi='vim'
alias vio='vim -O'
alias vv='vim -R'
alias wttr='curl wttr.in/~Krasnodar'

# Git handies
alias gs='git status'
alias gl="git log --color --graph --pretty=format:'%Cred%h%Creset -%C(yellow)%d%Creset %s %Cgreen(%cd) %C(bold blue)%an <%ae>%Creset' --abbrev-commit"
alias gf='git fetch'
alias gcb='git checkout -b'
alias gb='git branch'
alias gbr='git branch -r'
alias gc='git checkout'
alias ga='git add --all'
alias go='git commit'
alias gor='git commit --amend --reuse-message=HEAD'
alias goz='git reset HEAD^ ; git push origin +HEAD'
alias gp='git push -u origin HEAD'
alias gd='git diff --color'
alias gdca='git diff --color --cached'
alias gw='git branch --merged | grep -v "*" | grep -v "master" | xargs -n 1 git branch -d'
alias gri='git rebase -i'
alias grc='git rebase --continue'
alias gra='git rebase --abort'
alias gsu='git submodule update'
alias gsi='git submodule update --init --recursive'
alias gss='git stash save'
alias gsp='git stash pop'
alias gsc='git stash clear'
alias gu='git pull'
alias gup='git-up'

# Docker speed-up
alias dex='docker exec -it'
alias dim='docker images'
alias din='docker inspect'
alias dps='docker ps -a'
alias dri='docker rmi'
alias drm='docker rm'

# new aliases
alias fzf="fzf --cycle -i"
alias shk="cat ~/.ssh/id_rsa.pub"
alias setkb="setxkbmap -layout 'us,ru' && setxkbmap -option 'grp:caps_toggle'"
alias winon="sudo virsh resume win10"
alias winof="sudo virsh suspend win10"

sc(){
  local service
  [[ -f ~/.cache/systemctl-commands ]] || tee ~/.cache/systemctl-commands << EOF
status
start
stop
restart
enable
disable
edit
EOF
  action=$( cat ~/.cache/systemctl-commands | fzf ) && \
    service=$( systemctl | fzf | awk '{print $1}' ) && \
    sudo systemctl --no-pager $action $service
}

fk(){
  local pid
  pid=$( \ps -eo pid,user,cmd | fzf -i --layout=reverse-list --no-sort | awk '{print $1}' )
  sudo kill $pid
}

# terminal extensions
[[ $TERM = 'xterm-kitty' ]] && alias ssh='~/bin/kittyssh'

# last but not least: motd
alias | shuf -n 1
